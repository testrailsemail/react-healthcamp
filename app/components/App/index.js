import React from 'react';
import PropTypes from 'prop-types';

function App({ children }) {
  return (
    <div className='container' id='container'>
      <div className="text-center" id="columns">
        {children}
      </div>
    </div>
  )
}

App.propTypes = {
  children: PropTypes.node,
};

export default App;