import React from 'react';
// import { Link } from 'react-router-dom';

function Contacts () {
  return (
    <div id="contacts" className="contacts">
      <div className="overlay-b">
        <div className="container contact">
          <div className="col-xs-6 col-sm-6 col-md-3">
            <h6>Контакты</h6>
          </div>
          <div className="col-xs-offset-1 col-sm-offset-3 col-md-6 col-md-offset-0">
            <ul className="nospace linklist">
              <li>
                <i className="fa fa-phone"></i>
                <a href="tel:+380-66-413-0231">+38 (066) 413 0231</a>
              </li>
              <li>
                <i className="fa fa-envelope-o"></i>
                <a href="mailto:info@healthcamp.com.ua">info@healthcamp.com.ua</a>
              </li>
            </ul>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-3">
            <h6>Социальные сети</h6>
            <ul className="linklist social col-xs-offset-1 col-sm-offset-3 col-md-offset-1" 
                id="social">
              <li>
                <i aria-hidden="true" className="fa fa-facebook-official"></i>
                <a href="https://www.facebook.com/kiev.healthcamp">
                  www.facebook.com/kiev.healthcamp
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

module.exports = Contacts;