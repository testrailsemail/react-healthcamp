import React from 'react';
// var Link = require('react-router-dom').Link;
import { NavLink } from 'react-router-dom';

function Footer () {
  return (
    <nav  className='footer' 
          role="navigation">
      <p>
        Copyright © 2016 
        <NavLink to='/'>
          {' Health Camp'}
        </NavLink>
      </p>
    </nav>
  )
}

module.exports = Footer;