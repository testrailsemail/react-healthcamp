// @flow
import React from 'react';
import { NavLink } from 'react-router-dom';
import { scrollTo } from '../Utils';  

class Nav extends React.Component {
  state: {
    toggle: boolean,
  };
  
  constructor() {
    super();
    
    this.state = {
        toggle: false,
    };
    this.toggleClass = this.toggleClass.bind(this);
  }

  toggleClass = function() {
    const currentState = this.state.toggle;
    this.setState({ toggle: !currentState });
  }

  render() {
    var active = this.state.toggle ? ' active': ''
    return (
      <nav  id='custom-bootstrap-menu' 
            className='custom-bootstrap-menu navbar navbar-default navbar-static-top' 
            role="navigation">
        <div className='container'>
          <div className='navbar-header'>
            <div className='nav navbar-nav' id='logo'> 
              <NavLink activeClassName='active' className='navbar-brand' to='/'>
                <h1> HealthCamp </h1>
              </NavLink>
              <p className='navbar-text'> Красота силы и здоровья</p>
            </div>
            <button className={"navbar-toggle collapsed" + active} 
                    data-target=".navbar-collapse" 
                    data-toggle="collapse"
                    onClick= {this.toggleClass}
                    type="button">
              <span className="sr-only">Toggle navigation</span>
              <div className="btn-menu">
                <dic className="stripes"></dic>
              </div>
            </button>
          </div>
          <div className="navbar-collapse collapse">
            <ul className="nav navbar-nav navbar-right">
              <li className="nav-item">
                <NavLink activeClassName='active' to='/project'>
                  о проекте
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink activeClassName='active' to='/services'>
                  услуги
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink activeClassName='active' to='/pages'>
                  статьи
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink  id="show-contancts" 
                          activeClassName='active' 
                          onClick={function (e) {
                                    e.preventDefault();
                                    scrollTo("contacts", 600) 
                                  }}
                          to='/#contacts'>
                  контакты
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    )
  }
}

module.exports = Nav;