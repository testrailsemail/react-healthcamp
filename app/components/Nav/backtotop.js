import React from 'react';
import { scrollTo } from '../Utils';

class Backtotop extends React.Component {
  constructor(){
    super();
    this.state = window.scrollY < 150 ? {isHide:true} : {isHide:false};
    this.hideBar = this.hideBar.bind(this)
  }

  hideBar(){
    let {isHide} = this.state
    window.scrollY > 150?
    isHide && this.setState({isHide:false})
    :
    !isHide && this.setState({isHide:true})
  }

  componentDidMount(){
    window.addEventListener('scroll', this.hideBar);
  }

  componentWillUnmount(){
    window.removeEventListener('scroll', this.hideBar);
  }
  
  render(){
    let classHide=this.state.isHide?'':'visible'
    return (
      <a 
        id='backtotop' href="#top"
        className={classHide} 
        onClick={
          function (e) {
            e.preventDefault();
            scrollTo(0, 500) 
          }}>
            <i className='fa fa-chevron-up'></i>
      </a> 
    )
  }
}

module.exports = Backtotop;