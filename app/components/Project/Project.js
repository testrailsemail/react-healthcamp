// @flow
import React from 'react';
import PropTypes from 'prop-types';
import Coach from './Coach';
import ReactMarkdown from 'react-markdown';
import Loading from '../Loading';

class Project extends React.Component {
  props: {
    project: Object,
    coachEntities: Object,
    coachIds: Array<string>,
  };
  
  render() {
    // var active = this.state.toggle ? ' active': ''
    // this.props.coachEntities ? const { coachEntities, } = this.props :
    const coaches = this.props.coachEntities;
    const project = this.props.project;
    return (
      <div className="main">
          <div id="description">
            {project ?
              <div className="text">
                <h1>{project.title}</h1>
                <ReactMarkdown softBreak="br" source={project.content} />
              </div>
            : 
            <Loading text="Загрузка" /> }
          </div>  
          {coaches[1] ? 
            <div id="coaches">
              <div className="text">
                <h1>Наши тренеры</h1>
                  {Object.keys(this.props.coachEntities)
                    .map((key) => <Coach key={coaches[key].name} coach={coaches[key]} />)
                  }
              </div>
            </div>
          :
          <Loading text="Загружаем" /> }
      </div>
    )
  }
}

Project.propTypes = {
  coachEntities: PropTypes.object,
  project: PropTypes.object,
  coachIds: PropTypes.arrayOf(PropTypes.number),
}

module.exports = Project;
