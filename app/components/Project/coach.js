import React from 'react';
import ReactMarkdown from 'react-markdown';
import PropTypes from 'prop-types';

class Coach extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const node = document.getElementById('fotoramajs');
    const script = document.createElement('script');
    node ? node.parentNode.removeChild(node) : (
      script.src = "src/lib/js/fotorama.js",
      script.id="fotoramajs",
      // script.async = true;
      document.body.appendChild(script)
    );
  }
  
  render() {
    const coach = this.props.coach;
    return (
      <div>
        <h2>{coach.name}</h2>
        <div className="breaker" 
          style={{clear:'both',height:'1px',lineHeight:'1px'}}>&nbsp;
        </div>
        <div>
          <p>
            <img alt={coach.name} 
              src={coach.image_url}/>
          </p>
          
          <ReactMarkdown softBreak="br" source={coach.content} />
           
        </div>
          
        <div className="breaker" 
            style={{clear:'both',height:'1px',lineHeight:'1px'}}>&nbsp;
        </div>
        <br/>
        <div className="fotorama"
              data-width="100%" data-ratio="960/640" 
              data-minwidth="400" data-maxwidth="960" 
              data-minheight="267" data-nav="thumbs" 
              data-loop="true" data-keyboard="true" 
              data-arrows="true" data-click="true" 
              data-swipe="true" data-thumbwidth="150" 
              data-thumbheight="100">
          {coach.pictures.map((picture) => {
            return( 
              <img key={picture} src={picture}/>
            )
          })}  
        </div>
      </div>
    )
  }
}

module.exports = Coach;

Coach.propTypes = {
  coach: PropTypes.shape({
    name: PropTypes.string,
    image_url: PropTypes.string,
    content: PropTypes.string,
    pictures: PropTypes.arrayOf(PropTypes.string)
  }),

}

// <div className="fotorama"
//   data-width="100%" data-ratio="960/640" 
//   data-minwidth="400" data-maxwidth="960" 
//   data-minheight="267" data-nav="thumbs" 
//   data-loop="true" data-keyboard="true" 
//   data-arrows="true" data-click="true" 
//   data-swipe="true" data-thumbwidth="150" 
//   data-thumbheight="100">
//   <img src="/src/images/phasol-02.jpg"/>
//   <img src="/src/images/phasol-03.jpg"/>
//   <img src="/src/images/phasol-04.jpg"/>
//   <img src="/src/images/phasol-05.jpg"/>
//   <img src="/src/images/phasol-06.jpg"/>
//   <img src="/src/images/phasol-07.jpg"/>
//   <img src="/src/images/phasol-08.jpg"/>
//   <img src="/src/images/phasol-09.jpg"/>
//   <img src="/src/images/phasol-10.jpg"/>
//   <img src="/src/images/phasol-11.jpg"/>
//   <img src="/src/images/phasol-12.jpg"/>
//   <img src="/src/images/phasol-13.jpg"/>
//   <img src="/src/images/phasol-14.jpg"/>
//   <img src="/src/images/phasol-15.jpg"/>
//   <img src="/src/images/phasol-16.jpg"/>
//   <img src="/src/images/phasol-17.jpg"/>
//   <img src="/src/images/phasol-18.jpg"/>
// </div>