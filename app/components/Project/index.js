/* eslint-disable no-unused-vars */
/*global React*/
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators as coachesActionCreators } from '../../ducks/auth';
// import { actionCreators as trackActionCreators } from '../../ducks/track';
import Project from './Project';

function mapStateToProps(state) {
  /* eslint-disable no-console */
  // console.log(state.coaches)
  const { coachEntities, coachIds, project } = state.auth;
  // console.log(coachEntities)
  return {
    coachEntities,
    coachIds,
    project
  }
}

function mapDispatchToProps(dispatch) {
  return {
    // onAuth: bindActionCreators(authActionCreators.doAuth, dispatch),
    // onPlay: bindActionCreators(trackActionCreators.doPlayTrack, dispatch),
    // onLike: bindActionCreators(trackActionCreators.doLikeTrack, dispatch),
  };
}

export default connect(mapStateToProps)(Project);