// @flow
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import Service from './_Service';
import ReactMarkdown from 'react-markdown';
import {FormattedNumber} from 'react-intl';
import Loading from '../Loading';
var Link = require('react-router-dom').Link;

class Service extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const node = document.getElementById('fotoramajs');
    const script = document.createElement('script');
    node ? node.parentNode.removeChild(node) : null;
    script.src = "/src/lib/js/fotorama.js";
    script.id="fotoramajs";
      // script.async = true;
    document.body.appendChild(script);

  }

  componentDidUpdate() {
    const node = document.getElementById('fotoramajs');
    const script = document.createElement('script');
    node ? node.parentNode.removeChild(node) : null;
    script.src = "/src/lib/js/fotorama.js";
    script.id="fotoramajs";
      // script.async = true;
    document.body.appendChild(script);
  }

  render() {
    /* eslint-disable no-console */
    // console.log(this.props);
    var id, service, pic, location;
    this.props.locationEntities ? (
          id = this.props.match.params.id,
          service = this.props.serviceEntities[id],
          pic = this.props.picEntities[service.pic],
          location = this.props.locationEntities[service.location]
    ) : null;
    
    // console.log(service);
    // console.log(pics);

    return (
      <div className="text">
        {service ? 
        <div>
          <h1>{service.name}</h1>
          <img src={"/" + pic.image} alt="Imgpsh fullsize" />
          <br/>
          <br/>
          <p className="desc">{service.description}</p>
          <ReactMarkdown softBreak="br" source={service.body} />
          <p>
            <strong>Цена:  </strong>
            <FormattedNumber 
              value={service.price} 
              style="currency" 
              currency="UAH"/>
          </p>
          <Link to="/services">Назад</Link>
          <div className="breaker" 
              style={{clear:'both',height:'1px',lineHeight:'1px'}}>&nbsp;
          </div>
          <div className="fotorama"
                data-width="100%" data-ratio="960/640" 
                data-minwidth="400" data-maxwidth="960" 
                data-minheight="267" data-nav="thumbs" 
                data-loop="true" data-keyboard="true" 
                data-arrows="true" data-click="true" 
                data-swipe="true" data-thumbwidth="150" 
                data-thumbheight="100">
            {location && location.pictures.map((picture) => {
              return( 
                <img key={picture} src={picture} />
              )
            })}
          </div>
        </div>
        :
        <Loading text="Загружаем" /> }
      </div>
    )
  }
}

Service.propTypes = {
  serviceEntities: PropTypes.object,
  locationEntities: PropTypes.object,
  picEntities: PropTypes.object,
  serviceIds: PropTypes.arrayOf(PropTypes.number),
  match: PropTypes.object,
}

// import { connect } from 'react-redux';
function mapStateToProps(state) {
  /* eslint-disable no-console */
  // console.log(state.coaches)
  const { serviceEntities, picEntities, serviceIds, locationEntities } = state.auth;
  // console.log(coachEntities)
  return {
    serviceEntities,
    locationEntities, 
    picEntities, 
    serviceIds
  }
}

module.exports = connect(mapStateToProps)(Service);