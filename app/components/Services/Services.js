// @flow
/* eslint-disable no-useless-escape */
import React from 'react';
import PropTypes from 'prop-types';
import _Service from './_Service';
// import ReactMarkdown from 'react-markdown';
import Loading from '../Loading';
// var Link = require('react-router-dom').Link;

function Services({ serviceEntities, picEntities, serviceIds }) {
  return (
    <div id="main">
      <h1 style={{textShadow: "rgba(0,0,0,0.7) 1px 1px 2px"}}>Услуги</h1>
      <div className="effects clearfix" id="effect">
        {/* eslint-disable no-console */
          console.log("serviceEnt: ", serviceEntities)}
        {serviceEntities ? 
          serviceIds.map((id, key) => {
            return (
              <div className="col-xs-12 col-sm-6 col-md-6" key={key}>
                <_Service 
                  service={serviceEntities[id]} 
                  pic={picEntities[serviceEntities[id].pic]} />
              </div>
            );
          })
        :
        <Loading text="Загружаем" /> }
      </div>
    </div>
  )
}

Services.propTypes = {
  serviceEntities: PropTypes.object,
  picEntities: PropTypes.object,
  serviceIds: PropTypes.arrayOf(PropTypes.number),
}

module.exports = Services;
