/* eslint-disable no-unused-vars */
import React from 'react';
// import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// import { actionCreators as coachesActionCreators } from '../../ducks/auth';
import Services from './Services';

function mapStateToProps(state) {
  /* eslint-disable no-console */
  // console.log(state.coaches)
  const { serviceEntities, picEntities, serviceIds } = state.auth;
  // console.log(coachEntities)
  return {
    serviceEntities, 
    picEntities, 
    serviceIds
  }
}

function mapDispatchToProps(dispatch) {
  return {
    // onAuth: bindActionCreators(authActionCreators.doAuth, dispatch),
    // onPlay: bindActionCreators(trackActionCreators.doPlayTrack, dispatch),
    // onLike: bindActionCreators(trackActionCreators.doLikeTrack, dispatch),
  };
}

export default connect(mapStateToProps)(Services);