export default function scrollTo(to, duration, easing = 'easeInOutQuint') {
  // https://stackoverflow.com/questions/8917921/cross-browser-javascript-not-jquery-scroll-to-top-animation/8918062#8918062
  // http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
  var requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
  })();
  if (duration <= 0) return;
  var scrollY = window.scrollY || document.documentElement.scrollTop, 
      // scrollTargetY = document.getElementById(to).offsetTop,
      scrollTargetY = "",
      // element = document.body,
  // var difference = scrollTargetY - scrollY;
  // var perTick = difference / duration * 10;
      currentTime = 0;

  if (to.toFixed) {
    scrollTargetY = to;
  } else {
    scrollTargetY = document.getElementById(to).offsetTop;
  }

  // min time .1, max time .8 seconds
  var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / duration, .8));
  
  // easing equations from https://github.com/danro/easing-js/blob/master/easing.js
  var easingEquations = {
      easeOutSine: function (pos) {
          return Math.sin(pos * (Math.PI / 2));
      },
      easeInOutSine: function (pos) {
          return (-0.5 * (Math.cos(Math.PI * pos) - 1));
      },
      easeInOutQuint: function (pos) {
          if ((pos /= 0.5) < 1) {
              return 0.5 * Math.pow(pos, 5);
          }
          return 0.5 * (Math.pow((pos - 2), 5) + 2);
      }
  };

  // add animation loop
  function tick() {
    currentTime += 1 / 60;

    var p = currentTime / time;
    var t = easingEquations[easing](p);

    if (p < 1) {
        requestAnimFrame(tick);

        window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
    } else {
        // console.log('scroll done');
        window.scrollTo(0, scrollTargetY);
    }
  }

  // call it once to get started
  tick();
        
  // setTimeout(function() {
  //     element.scrollTop = element.scrollTop + perTick;
  //     if (element.scrollTop === to) return;
  //     scrollTo(to, duration - 10);
  // }, 10);
}