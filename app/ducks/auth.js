// @flow
/* eslint-disable no-console */

const COACHES_SET: string = 'auth/COACHES_SET';
const PROJECT_SET: string = 'auth/PROJECT_SET';
const SERVICES_SET: string = 'auth/SERVICES_SET';
// const AUTH: string = 'auth/AUTH';
// const SESSION_SET: string = 'auth/SESSION_SET'


// import { map } from 'lodash';
import { normalize } from 'normalizr';
import { coachSchema, serviceSchema } from '../schemas';
import * as api from '../api';
// import { actionCreators as trackActionCreators } from './track';

function doSetCoaches(coachEntities, coachIds) {
  return {
    type: COACHES_SET,
    coachEntities,
    coachIds
  };
}

function doSetProject(project) {
  return {
    type: PROJECT_SET,
    project
  };
}

function doSetServices(serviceEntities, picEntities, locationEntities, serviceIds) {
  return {
    type: SERVICES_SET,
    serviceEntities,
    picEntities,
    locationEntities,
    serviceIds
  };
}

function doAuth() {
  return function (dispatch: Function) {
    api.connect().then((session) => {
      // console.log("session:", session);
      dispatch(doFetchCoaches(session));
      dispatch(doFetchProject(session));
      dispatch(doFetchServices(session));
    });
  };
}

type CoachesData = Array<Coach>;

function doFetchCoaches(session) {
  return function (dispatch: Function) {
    api.fetch(session.fetchCoaches)
      // .then((response) => response.json())
      .then((data: CoachesData) => {
        // console.log("Coaches data:", data);
        const normalized = normalize(data, [coachSchema]);
        // console.log("Coaches data:", normalized);
        dispatch(doSetCoaches(normalized.entities.coaches, normalized.result));
      });
  };
}

function doFetchProject(session) {
  return function (dispatch: Function) {
    api.fetch(session.fetchProject)
      // .then((response) => response.json())
      .then((data: Project) => {
        // console.log("data:", data);
        dispatch(doSetProject(data));
      });
  };
}

function doFetchServices(session) {
  return function (dispatch: Function) {
    api.fetch(session.fetchServices)
      // .then((response) => response.json())
      .then((data: Services) => {
        console.log("data:", data);
        const normalized = normalize(data, [serviceSchema]);
        console.log(normalized);
        dispatch(doSetServices(
                                normalized.entities.services, 
                                normalized.entities.pics,
                                normalized.entities.locations,
                                normalized.result)
        );
      });
  };
}

// function doFetchStream(session) {
//  return function (dispatch: Function) {
//     // fetch(`//api.soundcloud.com/me/activities?limit=20&offset=0&oauth_token=${session.oauth_token}`)
//     api.fetch(session.fetchUser)
//       // .then((response) => response.json())
//       .then((data: StreamData) => {
//         const normalized = normalize(map(data.collection, 'origin'), [trackSchema]);
//         /* eslint-disable no-console */
//         console.log(normalized);
//         // console.log(map(data.collection, 'origin'));
//         dispatch(trackActionCreators.doSetTracks(normalized.entities.tracks, normalized.result));
//       });
//   };
// }

type State = {
  auth?: Object;
};
// type SetUserAction = {
//     type: string;
//     user: User;
// };

const initialState = {
  coachEntities: {},
  coachIds: [],
};

function reducer(state: State = initialState, action: Object): State {
  switch (action.type) {
    case COACHES_SET:
      return applySetCoaches(state, action);
    case PROJECT_SET:
      return applySetProject(state, action);
    case SERVICES_SET:
      return applySetServices(state, action);
  }
  return state;
}

function applySetCoaches(state, action: SetCoachesAction) {
  const { coachEntities, coachIds } = action;
  return { ...state, coachEntities, coachIds };
}

function applySetProject(state, action: SetProjectAction) {
  const { project } = action;
  return { ...state, project };
}

function applySetServices(state, action: SetServicesAction) {
  const { serviceEntities, picEntities, locationEntities, serviceIds } = action;
  return { ...state, serviceEntities, picEntities, locationEntities, serviceIds };
}

const actionCreators = {
  doAuth,
};

const actionTypes = {
  COACHES_SET,
  PROJECT_SET,
  SERVICES_SET
};

export {
  actionCreators,
  actionTypes
};

export default reducer;