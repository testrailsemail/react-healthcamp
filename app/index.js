/* eslint-disable no-unused-vars */
import React from 'react';
import ReactDOM from 'react-dom';
import { Route} from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { ConnectedRouter  } from 'react-router-redux';
import { Provider } from 'react-redux';
import {IntlProvider} from 'react-intl';
import {addLocaleData} from 'react-intl';
import uk from 'react-intl/locale-data/uk';
import configureStore from './stores/configureStore';
import App from './components/App';
import Nav from './components/Nav/Nav';
import Backtotop from './components/Nav/Backtotop';
import Pages from './components/Pages';
import Project from './components/Project';
import Services from './components/Services';
import Service from './components/Services/Service';
import Footer from './components/Footer';
import Contacts from './components/Contacts';

import { actionCreators as coachesActionCreators } from './ducks/auth';
// import style from './stylesheets/main.scss';
require('./stylesheets/main.scss');

const store = configureStore();
store.dispatch(coachesActionCreators.doAuth());

const history = createBrowserHistory()

addLocaleData([...uk]);

ReactDOM.render(
  <IntlProvider locale="uk">
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div className='app-body'>
        <Nav />
        <Backtotop />
        <App>
          <Route exact path="/" component={Pages} />
          <Route path="/project" component={Project} />
          <Route exact path="/services" component={Services} />
          <Route path="/services/:id" component={Service} />
          <Route path="/pages" component={Pages} />
        </App>
        <Contacts />
        <Footer />
      </div>
    </ConnectedRouter>
  </Provider>
  </IntlProvider>,
  document.querySelector('.app')
);

module.hot.accept();