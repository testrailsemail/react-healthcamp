import coachSchema from './coach';
import serviceSchema from './services';
import locationSchema from './location';

export {
  coachSchema,
  serviceSchema,
  locationSchema
};