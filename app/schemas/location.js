import { schema } from 'normalizr';

/* eslint-disable no-console */
const locationSchema = new schema.Entity('locations');

export default locationSchema;