import { schema } from 'normalizr';
import picSchema from './pic';
import locationSchema from './location';

/* eslint-disable no-console */

const serviceSchema = new schema.Entity('services');
serviceSchema.define({
  pic: picSchema,
  location: locationSchema
});


export default serviceSchema;