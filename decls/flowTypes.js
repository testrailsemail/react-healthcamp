declare type Coach = {
  id: string;
  image_url: string;
  name: string;
  pictures?: ?Array<string>;
};

declare type Project = {
  title: string;
  content: string;
};

declare type SetCoachesAction = {
    type: string;
    coachIds: Array<string>;
    coachEntities: Object;
};

declare type SetProjectAction = {
    type: string;
    project: Object;
};

// declare type LikeTrackAction = {
//     type: string;
//     trackId: string;
//     trackEntities: Object;
// };

declare type Action = SetCoachesAction | SetProjectAction;