var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: [
  	'webpack-dev-server/client?http://localhost:8082',
    'webpack/hot/only-dev-server',
    './app/index.js'
  ],
  output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'index_bundle.js',
		publicPath: '/'
	},
	module: {
    rules: [
			{ test: /\.(js)$/, use: 'babel-loader', exclude: /node_modules/ },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel-loader', 'eslint-loader']
      },
      {test:/\.scss$/, exclude: /node_modules/, loaders:['style-loader','css-loader','sass-loader']},
			// { test: /\.css$/, use: [ 'style-loader', 'css-loader' ]}
		]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    // enable HMR globally

    new webpack.NamedModulesPlugin(),
    // prints more readable module names in the browser console on HMR updates

    new webpack.NoEmitOnErrorsPlugin(),
    // do not emit compiled assets that include errors
  ],  
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  devServer: {
    contentBase: './dist',
    hot: true,
    historyApiFallback: true
  }
};